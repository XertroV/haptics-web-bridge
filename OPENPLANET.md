This bridge allows [Haptics](https://openplanet.dev/plugin/haptics) plugin communicate with device server by creating a local web-server and translating requests into *REST API* which then can be read by *Openplanet* itself.

## Disclaimer
This is a temporary solution and will be removed when *WebSockets* are introduced to *Openplanet*.

## Source Code
You can access source code on [GitLab](https://gitlab.com/fentrasLABS/openplanet/haptics-web-bridge).

## Credits
Originally developed by [Kyrah Abattoir](https://gitlab.com/KyrahAbattoir).