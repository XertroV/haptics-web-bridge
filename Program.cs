/* SPDX-License-Identifier: CC-BY-NC-SA-4.0 */
using HapticsWebBridge.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace HapticsWebBridge
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://localhost:8008");
                })
                .ConfigureServices(services =>
                {
                    services.AddSingleton<DeviceRegister>();
                    services.AddSingleton<BridgeSettings>();
                    services.AddHostedService<WebsocketService>();
                    services.AddControllers().AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.PropertyNamingPolicy = null;
                    });
                });
    }
}
